<?php
ini_set('xdebug.max_nesting_level', 4096);
require  'Graph.php';

$file = fopen("input", "r");
$testCount = fgets($file);

for ($test = 0; $test < $testCount; $test++) {
    preg_match('/^n=(?<nodes>[0-9]+),m=(?<edge>[0-9]+)$/', fgets($file), $matches);

    $graph = new Graph($matches['nodes']);

    foreach (explode(' ', fgets($file)) as $edge) {
        if (preg_match('/^{(?<from>[0-9]+),(?<to>[0-9]+)}$/', $edge, $matches)) {
            $graph->add($matches['from'], $matches['to']);
        }
    }

    $graph->dfsEuler(0)->getResult();

    echo PHP_EOL;
}
