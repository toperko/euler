<?php

/**
 * Class Graph
 */
class Graph
{
    /**
     * @var int
     */
    private $nodes;

    /**
     * @var int[][]
     */
    private $matrix;

    /**
     * @var int[]
     */
    private $result = [];

    /**
     * Graph constructor.
     *
     * @param int $nodes
     */
    public function __construct(int $nodes)
    {
        $this->nodes = $nodes;
        for ($x = 0; $x < $nodes; $x++)
            for ($y = 0; $y < $nodes; $y++)
                $this->matrix[$x][$y] = 0;
    }

    /**
     * @param int $from
     * @param int $to
     *
     * @return Graph
     */
    public function add(int $from, int $to) : self
    {
        $this->matrix[$from][$to]++;
        $this->matrix[$to][$from]++;

        return $this;
    }

    public function getResult() : void
    {
        foreach ($this->result as $item) {
            echo $item . ' ';
        }
        echo PHP_EOL;
    }

    /**
     * @param int $startEdge
     *
     * @return Graph
     */
    public function dfsEuler(int $startEdge) : self
    {
        for ($i = 0; $i < $this->nodes; $i++) {
            while ($this->matrix[$startEdge][$i]) {
                $this->matrix[$startEdge][$i]--;
                $this->matrix[$i][$startEdge]--;
                $this->dfsEuler($i);
            }
        }
        $this->result[] = $startEdge;

        return $this;
    }
}
